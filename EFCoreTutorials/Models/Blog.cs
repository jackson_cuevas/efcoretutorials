﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreTutorials.Models
{
    public class Blog
    {
        public int BlogID { get; set; }
        public string Url { get; set; }
        public List<Post> Posts { get; set; }
    }
}
