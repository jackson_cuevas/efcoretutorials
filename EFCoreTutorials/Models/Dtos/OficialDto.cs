﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreTutorials.Models.Dtos
{
    public class OficialDto
    {
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public int Codigo { get; set; }
        public string Cargo { get; set; }
        public int Rol { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string Extension { get; set; }
        public int IdAdministracion { get; set; }
    }
}
