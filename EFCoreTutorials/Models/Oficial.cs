using System.ComponentModel.DataAnnotations;

namespace EFCoreTutorials.Models
{
    public class Oficial
    {
        [Key] 
        public string CEDULA_OFICIAL { get; set; }
        public string NOMBRE_OFICIAL { get; set; }
        public int CODIGO_EMPLEADO { get; set; }
        public string CARGO_OFICIAL { get; set; }
        public int ROL_OFICIAL { get; set; }
        public string TELEFONO_OFICIAL { get; set; }
        public string CORREO_OFICIAL { get; set; }
        public string EXTENSION_OFICIAL { get; set; }
        public int ID_ADMINISTRACION { get; set; }
    }
}