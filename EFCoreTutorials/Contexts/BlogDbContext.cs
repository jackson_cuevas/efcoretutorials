﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreTutorials.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCoreTutorials.Contexts {
    public class BlogDbContext : DbContext {
        public BlogDbContext (DbContextOptions<BlogDbContext> options) : base (options) {

        }

        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Oficial> Oficiales { get; set; }

    }

}