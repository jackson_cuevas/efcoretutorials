﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreTutorials.Contracts
{
    public interface IRepositoryWrapper
    {
        IBlogRepository Blog { get; }
        void Save();
    }
}
