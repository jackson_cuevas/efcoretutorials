﻿using EFCoreTutorials.Models;
using EFCoreTutorials.Repository;
using System.Collections.Generic;

namespace EFCoreTutorials.Contracts
{
    public interface IBlogRepository: IRepositoryBase<Oficial>
    {
        ISet<Oficial> GellAllSP();

    }
}
