﻿using EFCoreTutorials.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EFCoreTutorials.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        private readonly BlogDbContext _context;
        public RepositoryBase(BlogDbContext context)
        {
            _context = context;
        }

        void IRepositoryBase<T>.Create(T entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

        void IRepositoryBase<T>.Delete(T entity)
        {
            _context.Remove(entity);
        }

        ISet<T> IRepositoryBase<T>.FindAll()
        {
            return _context.Set<T>().ToHashSet();
        }

        T IRepositoryBase<T>.FindByCondition(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().FirstOrDefault(expression);
        }

        void IRepositoryBase<T>.Update(T entity)
        {
            _context.Update(entity);
        }
    }
}
