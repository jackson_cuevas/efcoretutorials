﻿using EFCoreTutorials.Contexts;
using EFCoreTutorials.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreTutorials.Repository
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private BlogDbContext _repoContext;
        private IBlogRepository _blogs;

        public IBlogRepository Blog
        {
            get
            {
                if (_blogs == null)
                {
                    _blogs = new BlogRepository(_repoContext);
                }

                return _blogs;
            }
        }

        public RepositoryWrapper(BlogDbContext context)
        {
            _repoContext = context;
        }

        void IRepositoryWrapper.Save()
        {
            _repoContext.SaveChanges();
        }
    }
}
