﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using EFCoreTutorials.Contexts;
using EFCoreTutorials.Contracts;
using EFCoreTutorials.Models;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;

namespace EFCoreTutorials.Repository
{
    public class BlogRepository: RepositoryBase<Oficial>, IBlogRepository
    {
        private readonly BlogDbContext _context;
        public BlogRepository(BlogDbContext context) :base(context)
        {
            _context = context;
        }

        ISet<Oficial> IBlogRepository.GellAllSP()
        {
            var alloficiales = new OracleParameter("pCur", OracleDbType.RefCursor, ParameterDirection.Output);

            var datos = _context.Oficiales
                .FromSql("BEGIN PKG_AOF_ASIGNACION.GET_OFICIALES('1',:pCur); END;", new object[] { alloficiales })
                .ToHashSet();

            return datos;
        }

    }
}
