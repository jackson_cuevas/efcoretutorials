﻿using AutoMapper;
using EFCoreTutorials.Models;
using EFCoreTutorials.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreTutorials.AutoMapper
{
    public class OrganizationProfile : Profile
    {
        public OrganizationProfile()
        {
            CreateMap<Oficial, OficialDto>()
                .ForMember(dto => dto.Cedula, opt => opt.MapFrom(obj => obj.CEDULA_OFICIAL))
                .ForMember(dto => dto.Nombre, opt => opt.MapFrom(obj => obj.NOMBRE_OFICIAL))
                .ForMember(dto => dto.Codigo, opt => opt.MapFrom(obj => obj.CODIGO_EMPLEADO))
                .ForMember(dto => dto.Cargo, opt => opt.MapFrom(obj => obj.CARGO_OFICIAL))
                .ForMember(dto => dto.Rol, opt => opt.MapFrom(obj => obj.ROL_OFICIAL))
                .ForMember(dto => dto.Telefono, opt => opt.MapFrom(obj => obj.TELEFONO_OFICIAL))
                .ForMember(dto => dto.Correo, opt => opt.MapFrom(obj => obj.CORREO_OFICIAL))
                .ForMember(dto => dto.Extension, opt => opt.MapFrom(obj => obj.EXTENSION_OFICIAL))
                .ForMember(dto => dto.IdAdministracion, opt => opt.MapFrom(obj => obj.ID_ADMINISTRACION));
        }
    }
}
