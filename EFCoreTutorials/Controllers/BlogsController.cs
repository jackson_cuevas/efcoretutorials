﻿using EFCoreTutorials.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EFCoreTutorials.Models.Dtos;

namespace EFCoreTutorials.Controllers
{
	[Route("api/[controller]")]
    [ApiController]
    public class BlogsController
    {
        private IRepositoryWrapper _repository;
        private readonly IMapper _mapper;

        public BlogsController(IRepositoryWrapper repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<OficialDto>> Get()
        {
            var blog = _repository.Blog.GellAllSP().ToHashSet();
            var blogs = _mapper.Map<ISet<OficialDto>>(blog).ToHashSet();

            return blogs;
        }
    }
}
